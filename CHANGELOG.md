# Changelog

## 0.10.0 - 2025-02-26

### Removed

  - Dropped support for Python 3.8
  - Dropped support for Go 1.22

### Added

  - Added support for Python 3.12
  - Added support for Go 1.24

## 0.9.0 - 2024-10-27

### Changed

  - **Breaking:** Move to using inputs over variables for configurations
  - **Breaking:** Moved configurations under `./config`
  - At `-race` flag to Go test flags

## 0.8.0 - 2024-10-10

### Changed

  - Drop Go 1.21 in testing
  - Update `go-cov` for testing to `v0.4.0`
  - Enforce coverage at 100% for testing

### Added

  - Add Go 1.23 in testing

## 0.7.0 - 2024-03-27

### Changed

  - Add better reporting of Go code coverage with the `-coverpkg` flag
  - Add workaround for bug with `-coverpkg` flag

## 0.6.0 - 2024-02-22

### Changed

  - Allow skipping a Go version in tests
  - Drop Go 1.20 from test matrix
  - Add Go 1.22 to test matrix

## 0.5.0 - 2023-08-08

### Changed

  - Add Go 1.21 to test matrix for `go-test`
  - Drop Go 1.19 from test matrix
  - Drop Python 3.7 from test matrix for `pytest`

## 0.4.0 - 2023-07-15

### Changed

  - Removed `before_script` from `pre-commit`

## 0.3.0 - 2023-07-03

### Added

  - Add `SKIP` variable for `pre-commit`

## 0.2.0 - 2023-07-02

### Added

  - Add `go-test` configuration
  - Add handling for `golangci-lint` via `pre-commit`
  - Add support for reporting Go coverage via
    [`go-cov`](https://gitlab.com/matthewhughes/go-cov)

## 0.1.0 - 2023-05-07

Initial release
