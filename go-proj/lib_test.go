package proj

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_foo(t *testing.T) {
	require.Equal(t, foo(), 12)
}
