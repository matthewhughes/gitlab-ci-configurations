import pytest

from lib import add


@pytest.mark.parametrize(
    ("args", "expected"),
    (((1,), 1), ((1, 2), 3), ((-1, 1), 0)),
)
def test_add(args, expected):
    assert add(*args) == expected
